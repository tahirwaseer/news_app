/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
  Component,
  NavigatorIOS,
  StyleSheet,
  Dimensions,
  Image,
  TouchableHighlight,
  Text,
  ScrollView,
  View
} from 'react-native';
import Login from './Login';
import navHelper from '../helpers/navigation';
let width = Dimensions.get('window').width;

let height = Dimensions.get('window').height;

class LoginScreen extends Component {
  _navigate(route) {
    if(route.newView){
//       this._navigator.push(navHelper(route.routeName));
    }else{
      this._navigator.replace({
            title: route.routeName,
            component: Home,
            leftButtonIcon: require('../images/menu.png'),
            onLeftButtonPress: () => { this._drawer.open() }
        });
    }
    this._drawer.close();
  }
  _login(){
    
    this.props.toRoute({
      name: "Home",
      component: Login
    });

  }
  render() {
    global.__user = false;
    return (
          <View style={{ flex:1, backgroundColor: 'transparent' }}>
            <View>
                <Image style={{ height: height, width: width, position: 'absolute', top:0, left:0 }} source={ require('../images/app-bg.jpg') } />
            </View>
            <View style={styles.container}>
              <View style={{flex: 3,justifyContent:'center',alignItems:'center'}}>
                <Text style={styles.title}>NOOZOO</Text>
                <Text style={styles.instructions}>High quality. Delivered.</Text>
              </View>
              <View style={styles.buttonsContainer}>
                <TouchableHighlight style={styles.button}
                                    underlayColor='#096de8'
                                    onPress={this._login.bind(this)}>
                    <Text style={styles.buttonText}>Guest</Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button}
                                    underlayColor='#096de8'
                                    onPress={this._login.bind(this)}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableHighlight>
              </View> 
            </View>
          </View>
//       <Login/>
//       <Drawer
//         ref={(ref) => this._drawer = ref}
//         type="displace"
//         content={<Menu navigate={(route) => { this._navigate(route)} }/>}
//         tapToClose={true}
//         openDrawerOffset={0.2}
//         panCloseMask={0.2}
//         closedDrawerOffset={-3}
//         styles={{
//                 drawer: {},
//                 main: {paddingLeft: 0}
//         }}
//         tweenHandler={(ratio) => ({
//                 main: { opacity:(2-ratio)/2 }
//         })}>
//         <NavigatorIOS
//         barTintColor="#000000"
//         tintColor="#fff"
//         titleTextColor="#fff"
//         ref={(ref) => this._navigator = ref}
//         style={{flex: 1,backgroundColor: '#fff'}}
//         initialRoute={{
//             title: 'Login',
//             component: Login,
//             leftButtonIcon: require('./images/menu.png'),
//             onLeftButtonPress: () => { this._drawer.open() }
//         }}/>
//     </Drawer>
    );
  }
}
var fontBold = 'HelveticaNeue-Bold';
var fontNormal = 'Helvetica Neue';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  listWraper: {
      backgroundColor: 'rgba(0,0,0,0.2)'
  },
  buttonsContainer: {
    flex: 1,flexDirection:'row',
    alignItems:'stretch',
    padding: 20,
  },  
  button: {
      flex: 1,
      height: 40,
      padding: 5,
      backgroundColor: '#3d8ee5',
      borderRadius: 1,
      justifyContent: 'center',
      marginTop: 15,
      marginLeft: 5,
      marginRight: 5,
  },
  title: {
      fontSize: 36,
      fontFamily: fontBold,
      color: '#FFFFFF'
  },
  buttonText: {
      fontSize: 18,
      color: 'white',
      fontFamily: fontNormal,
      alignSelf: 'center'
  },
  instructions: {
    textAlign: 'center',
    color: '#ddd',
    fontFamily: fontNormal,
    marginBottom: 5,
  },
});

export default LoginScreen
