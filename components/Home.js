'use strict';

var React = require('react-native');

var {
    Image,
    StyleSheet,
    Text,
    View,
    Component,
    Dimensions,
    RefreshControl,
    ListView,
    TouchableHighlight,
    ActivityIndicatorIOS
   } = React;
   var width = Dimensions.get('window').width; //full width
   var height = Dimensions.get('window').height; //full height
   var fontBold = 'HelveticaNeue-Bold';
   var fontNormal = 'Helvetica Neue';
var styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        padding: 10,
    },
    
    separator: {
       height: 1,
       backgroundColor: '#dddddd'
    },
    pageTitleWraper: {flex: 0,marginTop: 60,padding: 10,backgroundColor: '#333'},
    pageTitle: {fontSize: 16, color: '#ffff',alignSelf: 'center'},
    thumbnailWraper: {
       flex:1
     },
  
    thumbnail: {
      width: (width/4)-10,
      height: 90,
     },
     listView: {
      backgroundColor: '#FFFFFF',
     },
     loading: {
         flex: 1,
         alignItems: 'center',
         justifyContent: 'center'
     },
    itemContent: {
      flex: 3,
      marginLeft: 10,
      flexDirection: 'column',
    },

    title: {
      flex: 2,
      fontSize: 15,
      fontFamily: fontBold,
      fontWeight: 'bold',
    },
    itemIcons: {
       flex: 1,
       flexDirection: 'row',
       alignItems: 'flex-end',
       marginTop: 20
    },
    iconsImage: {
      height: 30,
      width: 40,
      marginRight: 10,
      alignSelf: 'flex-end'

    },
    iconsText: {
      fontFamily: fontNormal,
      fontSize: 15,
      color: "#303030",
      alignSelf: 'flex-end'
    },
    companyView: {flex:1,flexDirection:'row'},
    dateView: {flex:1,alignSelf: 'flex-end'},
    iconsTextRight: {
      fontSize: 13,
      color: "#303030",
      fontFamily: fontNormal,
      alignSelf: 'flex-end'
    },
   listItem: {
     flex: 1,
     flexDirection: 'row',
     justifyContent: 'center',
     alignItems: 'center',
     padding: 13,
    //  backgroundColor: "#26BF45"
   }
});

var REQUEST_URL = 'http://accenture-feed.herokuapp.com/api/items';
var NewsDetail = require('./NewsDetail');
class HomeScreen extends Component {
    constructor(props) {
       super(props);
       this.state = {
           page: 1,
           isLoading: true,
           refreshing: false,
           isLoadingMore: false,
           dataSource: new ListView.DataSource({
               rowHasChanged: (row1, row2) => row1 !== row2
           })
       };
    }
    
    componentDidMount() {
      console.log('componentDidMount');
      if (global.__newsLoaded) {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(global.__newsData),
            isLoading: false
        });
      }else {
        this.fetchData(this.state.page);
      }
    }
  
    loadMore() {
      this.setState({isLoadingMore: true});
      console.log('end reaced');
      var page = this.state.page + 1;
      this.setState({page: page});
      this.fetchData(this.state.page);
    }
    _onRefresh() { 
      this.setState({refreshing: true,page:1}); 
      this.fetchData(this.state.page,false);
    }
    fetchData(page,append=true) {
      var token = this.props.token;
      var url = REQUEST_URL+"?token="+token+'&page='+page+'&limit=15';
       fetch(url)
       .then((response) => response.json())
       .then((responseData) => {
           var newData = responseData.docs;
           if(this.state.data){
             if(append){
               newData= this.state.data.concat(newData);
             }
           }
           global.__newsData = newData;
           this.setState({
               data: newData,
               dataSource: this.state.dataSource.cloneWithRows(newData),
               isLoading: false,
               refreshing: false,
               isLoadingMore: false
           });
       })
       .done(()=>{global.__newsLoaded=true});
    }
    renderNews(news) {
      var thumbnail = news.image !='' ?  <Image source={{uri: news.image}} style={ styles.thumbnail} /> : <Image source={require('./no-image.png')} style={ styles.thumbnail} />;
//       var companyImage = news.category[0].sourceImage !='' ?  <Image source={{uri: news.category[0].sourceImage}} style={ styles.iconsImage} /> : <Image source={require('../images/cnbc.png')} style={ styles.iconsImage} />;
      var companyImage = <Image source={require('../images/cnbc.png')} style={ styles.iconsImage} />;
      return (
            <TouchableHighlight onPress={() => this.showNewsDetail(news)}  underlayColor='#dddddd'>
                <View>
                    <View style={styles.listItem}>
                      <View style={styles.thumbnailWraper}>
                           {thumbnail}
                      </View>
                      <View style={ styles.itemContent}>
                          <Text numberOfLines={2} style={styles.title}>{this.parseHtmlEnteties(news.title)}</Text>
                          <View style={styles.itemIcons}>
                            <View style={styles.companyView}>
                              {companyImage}
                              <Text style={styles.iconsText}>{news.category_name}</Text>
                            </View>
                            <View style={styles.dateView}>
                              <Text style={styles.iconsTextRight}>{news.pubDate.toUpperCase()}</Text>
                            </View>
                          </View>
                      </View>
                    </View>
                    <View style={styles.separator} />
                </View>
            </TouchableHighlight>
       );
    }
    showNewsDetail(news) {
       this.props.navigator.push({
           title: news.category[0].industry_name,
           component: NewsDetail,
           passProps: {news},
           backButtonTitle: ''
       });
    }
    render() {
       if (this.state.isLoading) {
           return this.renderLoadingView();
       }
       var spinner = this.state.isLoadingMore ?
            ( <ActivityIndicatorIOS
                hidden='true'
                size='large'/> ) :
            ( <View/>); 
       return (
           <View style={{flex:1}}>
            <View style={styles.pageTitleWraper}>
             <Text style={styles.pageTitle}>All News</Text>
            </View> 
            <ListView
                dataSource={this.state.dataSource}
                refreshControl={ <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} /> }
                renderRow={this.renderNews.bind(this)}
                style={styles.listView}
                onEndReachedThreshold={250} 
                onEndReached={this.loadMore.bind(this)}
                />
             {spinner}
           </View>
            
        );
    }
    _logOut(){
      alert('Logout');
      realm.write(()=>{
        realm.delete('User', {name: global.__user.name,token: global.__user.token});
      });
      this._navigate({'title':'Company','routeName':'Home','active':false,'iconUrl': require('../images/check.png'),'newView':false}); 
      var users = realm.objects('User');
    }
    renderLoadingView() {
        return (
            <View style={styles.loading}>
                <ActivityIndicatorIOS
                    size='large'/>
                <Text>
                    Loading news...
                </Text>
            </View>
        );
    }
    parseHtmlEnteties(str) {
      return str.replace(/&#([0-9]{0,9});/gi, function(match, numStr) {
          var num = parseInt(numStr, 10); // read num as normal number
          return String.fromCharCode(num);
      });
    }
}

module.exports = HomeScreen;
