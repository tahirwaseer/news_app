import React, { Component, } from 'react'
import { View,TouchableHighlight,Text,NavigatorIOS,Image,StyleSheet,Dimensions } from 'react-native'
var Auth0Lock = require('react-native-lock');
var lock = new Auth0Lock({clientId: 's66FAkFrcb86Rl9Bj7YY3BO6B9yTfHRi', domain: 'tahirwaseer.auth0.com'});
import Home from './Home';
import Drawer from 'react-native-drawer';
import Menu from './Menu';
import navHelper from '../helpers/navigation';
import LoginScreen from './LoginScreen';
const Realm = require('realm');
const UserSchema = {
  name: 'User',
  properties: {
    name:  'string',
    token: 'string',
  }
};
let realm = new Realm({schema: [UserSchema]});
var fontBold = 'HelveticaNeue-Bold';
var fontNormal = 'Helvetica Neue';
let width = Dimensions.get('window').width;

let height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  listWraper: {
      backgroundColor: 'rgba(0,0,0,0.2)'
  },
  buttonsContainer: {
    flex: 1,flexDirection:'row',
    alignItems:'stretch',
    padding: 20,
  },  
  button: {
      flex: 1,
      height: 40,
      padding: 5,
      backgroundColor: '#3d8ee5',
      borderRadius: 1,
      justifyContent: 'center',
      marginTop: 15,
      marginLeft: 5,
      marginRight: 5,
  },
  title: {
      fontSize: 36,
      fontFamily: fontBold,
      color: '#FFFFFF'
  },
  buttonText: {
      fontSize: 18,
      color: 'white',
      fontFamily: fontNormal,
      alignSelf: 'center'
  },
  instructions: {
    textAlign: 'center',
    color: '#ddd',
    fontFamily: fontNormal,
    marginBottom: 5,
  },
});
class Login extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {useAsGuest: false};
  }
  componentDidMount(){
    var users = realm.objects('User');
    if(users.length<1){
      this.setState({useAsGuest: true});
    }else{
      global.__user = realm.objects('User')[0];
      this.setState({useAsGuest: false});
    }
  }
  _login(){
    lock.show({closable:true}, (err, profile, token) => {
        if (err) {
          console.log(err);
          return;
        }
        // Authentication worked!
        realm.write(() => {
         realm.create('User', {name: profile.nickname,token: profile.userId});
        });
        this.setState({useAsGuest: false});
        global.__user = realm.objects('User')[0];
      });
  }
  _guestLogin(){
    this.setState({useAsGuest: false});
    global.__user = false;
  }
  _navigate(route) {
      if(route.logout){
        this._logout();
      }else{
        this._navigator.replace({
              title: 'NOOZOO',
              component: Home,
              leftButtonIcon: require('../images/menu.png'),
              onLeftButtonPress: () => { this._drawer.open() }
          });
        this._drawer.close();
      }
    }
  render() {
      console.log(this.state.useAsGuest);
      if (this.state.useAsGuest) {
        return (
          <View style={{ flex:1, backgroundColor: 'transparent' }}>
            <View>
                <Image style={{ height: height, width: width, position: 'absolute', top:0, left:0 }} source={ require('../images/app-bg.jpg') } />
            </View>
            <View style={styles.container}>
              <View style={{flex: 3,justifyContent:'center',alignItems:'center'}}>
                <Text style={styles.title}>NOOZOO</Text>
                <Text style={styles.instructions}>High quality. Delivered.</Text>
              </View>
              <View style={styles.buttonsContainer}>
                <TouchableHighlight style={styles.button}
                                    underlayColor='#096de8'
                                    onPress={this._guestLogin.bind(this)}>
                    <Text style={styles.buttonText}>Guest</Text>
                </TouchableHighlight>
                <TouchableHighlight style={styles.button}
                                    underlayColor='#096de8'
                                    onPress={this._login.bind(this)}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableHighlight>
              </View> 
            </View>
          </View>
          );
      }
      return (
          <Drawer
            ref={(ref) => this._drawer = ref}
            type="displace"
            content={<Menu navigate={(route) => { this._navigate(route)} }/>}
            tapToClose={true}
            openDrawerOffset={0.2}
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={{
                    drawer: {},
                    main: {paddingLeft: 0}
            }}
            tweenHandler={(ratio) => ({
                    main: { opacity:(2-ratio)/2 }
            })}>
            <NavigatorIOS
              barTintColor="#000000"
              tintColor="#fff"
              titleTextColor="#fff"
              ref={(ref) => this._navigator = ref}
              style={{flex: 1,backgroundColor: '#fff'}}
              initialRoute={{
                  title: 'NOOZOO',
                  component: Home,
                  leftButtonIcon: require('../images/menu.png'),
                  onLeftButtonPress: () => { this._drawer.open() },
              }}/>
            </Drawer>
    )
  }
  _logout() {
    var users = realm.objects('User');
    if (users.length>0 && users[0].isValid()){ 
      console.log(users[0].isValid());
      realm.write(()=>{
        realm.delete(users[0]);
      });
    };
    this.setState({useAsGuest: true});
  }

}

export default Login