import React, { Component, } from 'react'
import { View,Text } from 'react-native'

class AnotherComponent extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <View>
        <Text>Hello its another component!</Text>
      </View>
    )
  }
}

export default AnotherComponent