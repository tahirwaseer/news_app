import React, { Component} from 'react'
import { View,ListView,Text,Image,TextInput,StyleSheet,TouchableHighlight } from 'react-native'
import Button from 'react-native-button';
var _navigate;
class Menu extends Component {

  static propTypes = {}

  static defaultProps = {}

  constructor(props) {
    super(props);
    _navigate = this.props.navigate;
    this.state = {
        dataSource: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})
    };
  }
  componentDidMount() {
    this.setState({
        dataSource: this.state.dataSource.cloneWithRows([
          {'title':'Company','routeName':'Home','active':false,'iconUrl': require('../images/check.png'),'newView':false},
          {'title':'Date & Time','routeName':'Home','active':true,'iconUrl': require('../images/check.png'),'newView':false},
          {'title':'Popularity','routeName':'Home','active':false,'iconUrl': require('../images/check.png'),'newView':false},
          {'title':'Latest','routeName':'Home','active':false,'iconUrl': require('../images/latest.png'),'newView':true},
          {'title':'Favourites','routeName':'Home','active':false,'iconUrl': require('../images/favourite.png'),'newView':true},
          {'title':'FAQ & Contact','routeName':'Home','active':false,'iconUrl': require('../images/faq_and_contact.png'),'newView':true},
          {'title':'Settings','routeName':'Home','active':false,'iconUrl': require('../images/settings.png'),'newView':true},
          {'title':'Logout','routeName':'Home','active':false,'iconUrl': require('../images/logOut.png'),'newView':true,'logout': true}
        ])
    });
  }
  _renderMenuItem(item) {
    return(
      <TouchableHighlight onPress={()=> this._onItemSelect(item)}>
        <View style={styles.menuItem}>
          {this._loadIcon(item)}
          <View style={styles.menuLinkWraper}><Text style={styles.menuLink} >{item.title}</Text></View>
        </View>
      </TouchableHighlight>
    );
  }
  _loadIcon(item){
      var image = <View style={styles.menuIcon}/>;
      if(item.newView){
        image = <Image style={styles.menuIcon} source={item.iconUrl}/>;
      }else{
        if(item.active){
          image = <Image style={styles.menuIcon} source={item.iconUrl}/>;
        }
      } 
    return(image);
  }
  _onItemSelect(item) {
    global.__newsLoaded==false;
    item.active = true;
    _navigate(item);
  }


  render() {
    return (
        <View style={styles.drawerWraper}>
          <View style={styles.searchWraper}>
            <TextInput 
              style={styles.searchInput} 
              onChangeText={(text) => this.setState({text})} 
              value={this.state.text} 
              placeholder='Search'
              placeholderTextColor="white"
            />
        </View>
          <ListView
              style={styles.listView}
              dataSource={this.state.dataSource}
              renderRow={(item) => this._renderMenuItem(item)}
              renderHeader= {()=><Text style={styles.listViewHeader}>Sort By</Text>}
          />
        </View>
    );
  }
}
var styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 60
    },
    drawerWraper: {
      flex: 1,
      backgroundColor: '#222',
      paddingTop: 60
    },
    searchWraper: {
      padding: 10
    },
    searchInput: {
      height: 30, 
      borderRadius: 5,
      paddingLeft: 10,
      paddingRight: 10,
      borderColor: 'white', 
      color: 'white',
      fontSize: 12,
      backgroundColor: 'rgba(0,0,0,0)',
      borderWidth: 1
    },
    listViewHeader: {
      backgroundColor: "#444444",
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 2,
      paddingBottom: 2,
      fontSize: 18,
      color: '#888888',
    },
    listView: {
      
    },
    menuItem: {
      flex: 1,
      flexDirection: 'row',
      alignSelf: 'stretch',
      borderBottomWidth: 1,
      borderBottomColor: '#444444',
      padding: 10
    },
    menuIcon:{
      height: 15,
      width: 15,
      marginRight: 10
    },
    menuLinkWraper:{
      flex: 5,
    },
    menuLink: {
      color: '#ffffff',
      fontSize: 15,
      textAlign: 'left'
    },
});
export default Menu