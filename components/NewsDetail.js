'use strict';

var React = require('react-native');

var {
    StyleSheet,
    Text,
    View,
    Component,
    Image,
    Dimensions,
    ScrollView
   } = React;
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
var custom_height = Dimensions.get('window').height/2.5; //full height
var fontBold = 'HelveticaNeue-Bold';
var fontNormal = 'Helvetica Neue';
var styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
      flex: 2,
      flexDirection: 'column',
      marginTop: 5,
      padding: 15
    },
    image: {
        width: width+3,
        height: custom_height,
        flex: 1,
        marginTop: 20
        // padding: 10
    },
    title: {
      fontSize: 17,
      fontWeight: 'bold',
      color: '#414042',
      fontFamily: fontBold,
      marginBottom: 15
    },
    itemIcons: {
      flex: 1,
      flexDirection: 'row',
      alignSelf: 'stretch',
      justifyContent:'space-between',
      marginBottom: 15
    },
    iconsImage: {
      height: 30,
      width: 40,
      marginRight: 10,
    },
    iconsText: {
      fontSize: 13,
      color: "#303030",
      fontFamily: fontNormal,
      alignSelf: 'flex-end'
    },
    companyView: {flex:1,flexDirection:'row'},
    dateView: {flex:1,alignItems: 'flex-end'},
    iconsTextRight: {
      fontFamily: fontNormal,
      fontSize: 13,
      color: "#303030",
      alignSelf: 'flex-end'
    },
    description: {
        fontFamily: fontNormal,
        padding: 2,
        fontSize: 15,
        color: '#414042'
    }
});
class NewsDetail extends Component {
  render() {
    var news = this.props.news;
    var image = news.image !='' ?  <Image source={{uri: news.image}} style={ styles.image} /> : <Image source={require('./no-image.png')} style={ styles.image} />;
//     var companyImage = news.category[0].sourceImage !='' ?  <Image source={{uri: news.category[0].sourceImage}} style={ styles.iconsImage} /> : <Image source={require('../images/cnbc.png')} style={ styles.iconsImage} />;
    var companyImage = <Image source={require('../images/cnbc.png')} style={ styles.iconsImage} />;
    var description = (typeof news.description !== 'undefined') ? this.parseHtmlEnteties(news.description) : '';
    return (
      <ScrollView style={{backgroundColor: 'rgba(255,255,255,1)'}}>
          <View style={styles.container}>
              {image}
              <View style={styles.content}>  
                <Text style={styles.title}>{news.title}</Text>
                <View style={styles.itemIcons}>
                  <View  style={styles.companyView}>
                    {companyImage}
                    <Text style={styles.iconsText}>{news.category_name}</Text>
                  </View>
                  <Text style={styles.iconsTextRight}>{news.pubDate.toUpperCase()}</Text>
                  
                </View>
                <Text style={styles.description}>{description}</Text>
              </View>
          </View>
      </ScrollView>
    );
  }
  formatDate(dateString) {
      var date= new Date(dateString);
      return date.toLocaleDateString('en-US',{ year: 'numeric', month: 'short', day: 'numeric',hour:'numeric', minute:'numeric' });
    }
    parseHtmlEnteties(str) {
      return str.replace(/&#([0-9]{0,9});/gi, function(match, numStr) {
          var num = parseInt(numStr, 10); // read num as normal number
          return String.fromCharCode(num);
      });
    }
}

module.exports = NewsDetail;
