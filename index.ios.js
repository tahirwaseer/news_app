/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, {
  AppRegistry,
  Component,
  NavigatorIOS,
  StyleSheet,
  Dimensions,
  Image,
  TouchableHighlight,
  Text,
  ScrollView,
  View
} from 'react-native';
import Drawer from 'react-native-drawer';
import Menu from './components/Menu';
import Login from './components/Login';
import LoginScreen from './components/LoginScreen';
import navHelper from './helpers/navigation';
let width = Dimensions.get('window').width;

let height = Dimensions.get('window').height;

class NewsApp extends Component {
  _navigate(route) {
    if(route.newView){
//       this._navigator.push(navHelper(route.routeName));
    }else{
      this._navigator.replace({
            title: route.routeName,
            component: Home,
            leftButtonIcon: require('./images/menu.png'),
            onLeftButtonPress: () => { this._drawer.open() }
        });
    }
    this._drawer.close();
  }
  _login(){
    this.props.navigator.replace({
                        title: 'NOOZOO',
                        component: Login,
                        leftButtonIcon: require('./images/menu.png'),
                        onLeftButtonPress: () => { this._drawer.open() },
                        passProps: {token: responseData.token}
                    });
  }
  render() {
    global.__user = false;
    return (
          
          <Login/>
//       <Drawer
//         ref={(ref) => this._drawer = ref}
//         type="displace"
//         content={<Menu navigate={(route) => { this._navigate(route)} }/>}
//         tapToClose={true}
//         openDrawerOffset={0.2}
//         panCloseMask={0.2}
//         closedDrawerOffset={-3}
//         styles={{
//                 drawer: {},
//                 main: {paddingLeft: 0}
//         }}
//         tweenHandler={(ratio) => ({
//                 main: { opacity:(2-ratio)/2 }
//         })}>
//         <NavigatorIOS
//         barTintColor="#000000"
//         tintColor="#fff"
//         titleTextColor="#fff"
//         ref={(ref) => this._navigator = ref}
//         style={{flex: 1,backgroundColor: '#fff'}}
//         initialRoute={{
//             title: 'Login',
//             component: Login,
//             leftButtonIcon: require('./images/menu.png'),
//             onLeftButtonPress: () => { this._drawer.open() }
//         }}/>
//     </Drawer>
    );
  }
}
var fontBold = 'HelveticaNeue-Bold';
var fontNormal = 'Helvetica Neue';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  listWraper: {
      backgroundColor: 'rgba(0,0,0,0.2)'
    },
  buttonsContainer: {
    flex: 1,flexDirection:'row',
    alignItems:'stretch',
    padding: 20,
  },  
  button: {
      flex: 1,
      height: 40,
      padding: 5,
      backgroundColor: '#3d8ee5',
      borderRadius: 1,
      justifyContent: 'center',
      marginTop: 15,
      marginLeft: 5,
      marginRight: 5,
  },
  title: {
      fontSize: 36,
      fontFamily: fontBold,
      color: '#FFFFFF'
  },
  buttonText: {
      fontSize: 18,
      color: 'white',
      fontFamily: fontNormal,
      alignSelf: 'center'
  },
  instructions: {
    textAlign: 'center',
    color: '#ddd',
    fontFamily: fontNormal,
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('news_app', () => NewsApp);
